package com.cifo.worldweather.information;

/**
 * Created by Alumne on 27/10/2015.
 */
public class Ciudad_dias {


    // Los dias vendran marcados por +1, +2, +3...
    private int dia;
    private String simbolo_tiempo;
    private int temperatura;

    public Ciudad_dias(int dia, String simbolo_tiempo, int temperatura) {
        this.dia = dia;
        this.simbolo_tiempo = simbolo_tiempo;
        this.temperatura = temperatura;
    }

    public int getHora() {
        return dia;
    }

    public void setHora(int dia) {
        this.dia = dia;
    }

    public String getSimbolo_tiempo() {
        return simbolo_tiempo;
    }

    public void setSimbolo_tiempo(String simbolo_tiempo) {
        this.simbolo_tiempo = simbolo_tiempo;
    }

    public int getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(int temperatura) {
        this.temperatura = temperatura;
    }
}
