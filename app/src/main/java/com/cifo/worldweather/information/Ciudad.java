package com.cifo.worldweather.information;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Alumne on 27/10/2015.
 */
public class Ciudad {

    // Informacion.

    //Calendar calendario = Calendar.getInstance();
    Calendar calendario = new GregorianCalendar();

    // Clase con la informacion de la Ciudad.

    private int dia_actual;
    private int hora_actual;

    // ID
    private int ciudad_idbd;
    // ID del SERVIDOR
    private int ciudad_idservidor;

    // NOMBRE
    private String ciudad_nombre;

    // SUNRISE
    private String ciudad_sunrise;
    // SUNSET
    private String ciudad_sunset;


    // TEMPERATURA
    private String ciudad_temperatura;
    // TEMPERATURA MAXIMA
    private String ciudad_temperatura_maxima;
    // TEMPERATURA MINIMA
    private String ciudad_temperatura_minima;
    // HUMEDAD
    private String ciudad_humedad;
    // PRESION ATMOSFERICA
    private String ciudad_presion_atmosferica;
    // VIENTO
    private String ciudad_viento;
    // IMAGEN CIUDAD
    private int link_imagen_ciudad;
    // IMAGEN TIEMPO ACTUAL
    private int link_imagen_tiempo_actual;
    // FAVORITO!
    private int ciudad_favorito;


    // SIGUIENTES HORAS.
    ArrayList<Ciudad_horas> Ciudad_horas = null;
    // SIGUIENTES DIAS.
    ArrayList<Ciudad_dias> Ciudad_dias = null;

    // CONSTRUCTORA!
    public Ciudad(){

    }



    public Ciudad(int ciudad_id, String ciudad_nombre) {
        this.ciudad_idbd = ciudad_id;
        this.ciudad_nombre = ciudad_nombre;
    }


    //CONSTRUCTOR 2
    public Ciudad(int imgCiudad, String nombre, String temperatura, int icono) {
        this.link_imagen_ciudad = imgCiudad;
        this.ciudad_nombre = nombre;
        this.ciudad_temperatura = temperatura;
        this.link_imagen_tiempo_actual = icono;
    }



    public Ciudad(String ciudad_nombre) {
        this.ciudad_nombre = ciudad_nombre;
    }

    // Poner informacion.
    public Ciudad setCiudad_information(
            String ciudad_temperatura,
            String ciudad_temperatura_maxima,
            String ciudad_temperatura_minima,
            String ciudad_humedad,
            String ciudad_presion_atmosferica,
            String ciudad_viento) {

        // AL CREAR LA CIUDAD
        this.dia_actual = calendario.get(Calendar.DAY_OF_MONTH);
        this.hora_actual = calendario.get(Calendar.HOUR_OF_DAY);
        this.ciudad_temperatura = ciudad_temperatura;
        this.ciudad_temperatura_maxima = ciudad_temperatura_maxima;
        this.ciudad_temperatura_minima = ciudad_temperatura_minima;
        this.ciudad_humedad = ciudad_humedad;
        this.ciudad_presion_atmosferica = ciudad_presion_atmosferica;
        this.ciudad_viento = ciudad_viento;

        return this;
    }

    // Poner informacion con ArrayLists
    public Ciudad setCiudad_information(
            String ciudad_temperatura,
            String ciudad_temperatura_maxima,
            String ciudad_temperatura_minima,
            String ciudad_humedad,
            String ciudad_presion_atmosferica,
            String ciudad_viento,
            ArrayList<Ciudad_dias> Ciudad_dias,
            ArrayList<Ciudad_horas> Ciudad_horas
    ) {

        // AL CREAR LA CIUDAD
        this.dia_actual = calendario.get(Calendar.DAY_OF_MONTH);
        this.hora_actual = calendario.get(Calendar.HOUR_OF_DAY);
        this.ciudad_temperatura = ciudad_temperatura;
        this.ciudad_temperatura_maxima = ciudad_temperatura_maxima;
        this.ciudad_temperatura_minima = ciudad_temperatura_minima;
        this.ciudad_humedad = ciudad_humedad;
        this.ciudad_presion_atmosferica = ciudad_presion_atmosferica;
        this.ciudad_viento = ciudad_viento;
        this.Ciudad_dias = Ciudad_dias;
        this.Ciudad_horas = Ciudad_horas;

        return this;
    }


    // GETTERS Y SETTERS INDIVIDUALES.


    public int getCiudad_idbd() {
        return ciudad_idbd;
    }

    public void setCiudad_idbd(int ciudad_idbd) {
        this.ciudad_idbd = ciudad_idbd;
    }

    public int getCiudad_idservidor() {
        return ciudad_idservidor;
    }

    public void setCiudad_idservidor(int ciudad_idservidor) {
        this.ciudad_idservidor = ciudad_idservidor;
    }

    public String getCiudad_nombre() {
        return ciudad_nombre;
    }

    public void setCiudad_nombre(String ciudad_nombre) {
        this.ciudad_nombre = ciudad_nombre;
    }

    public String getCiudad_sunset() {
        return ciudad_sunset;
    }

    public void setCiudad_sunset(String ciudad_sunset) {
        this.ciudad_sunset = ciudad_sunset;
    }

    public String getCiudad_sunrise() {
        return ciudad_sunrise;
    }

    public void setCiudad_sunrise(String ciudad_sunrise) {
        this.ciudad_sunrise = ciudad_sunrise;
    }

    public String getCiudad_temperatura() {
        return ciudad_temperatura;
    }

    public void setCiudad_temperatura(String ciudad_temperatura) {
        this.ciudad_temperatura = ciudad_temperatura;
    }

    public String getCiudad_temperatura_maxima() {
        return ciudad_temperatura_maxima;
    }

    public void setCiudad_temperatura_maxima(String ciudad_temperatura_maxima) {
        this.ciudad_temperatura_maxima = ciudad_temperatura_maxima;
    }

    public String getCiudad_temperatura_minima() {
        return ciudad_temperatura_minima;
    }

    public void setCiudad_temperatura_minima(String ciudad_temperatura_minima) {
        this.ciudad_temperatura_minima = ciudad_temperatura_minima;
    }

    public String getCiudad_humedad() {
        return ciudad_humedad;
    }

    public void setCiudad_humedad(String ciudad_humedad) {
        this.ciudad_humedad = ciudad_humedad;
    }

    public String getCiudad_presion_atmosferica() {
        return ciudad_presion_atmosferica;
    }

    public void setCiudad_presion_atmosferica(String ciudad_presion_atmosferica) {
        this.ciudad_presion_atmosferica = ciudad_presion_atmosferica;
    }

    public String getCiudad_viento() {
        return ciudad_viento;
    }

    public void setCiudad_viento(String ciudad_viento) {
        this.ciudad_viento = ciudad_viento;
    }

    public int getCiudad_favorito() {
        return ciudad_favorito;
    }

    public void setCiudad_favorito(int ciudad_favorito) {
        this.ciudad_favorito = ciudad_favorito;
    }

    public ArrayList<Ciudad_horas> getCiudad_horas() {
        return Ciudad_horas;
    }

    // AÃ±ade un arrayList de Ciudad_horas
    public void setCiudad_horas(ArrayList<Ciudad_horas> Ciudad_horas) {
        this.Ciudad_horas = Ciudad_horas;
    }

    // AÃ±ade un objeto Ciudad_horas
    public void setCiudad_horas(Ciudad_horas Ciudad_horas) {
        this.Ciudad_horas.add(Ciudad_horas);
    }

    public ArrayList<Ciudad_dias> getCiudad_dias() {
        return Ciudad_dias;
    }

    // AÃ±ade un arrayList de Ciudad_dias
    public void setCiudad_dias(ArrayList<Ciudad_dias> Ciudad_dias) {
        this.Ciudad_dias = Ciudad_dias;
    }

    public void setCiudad_dias(Ciudad_dias Ciudad_dias) {
        this.Ciudad_dias.add(Ciudad_dias);
    }

    public int getLink_imagen_ciudad() {
        return link_imagen_ciudad;
    }

    public void setLink_imagen_ciudad(int link_imagen_ciudad) {
        this.link_imagen_ciudad = link_imagen_ciudad;
    }

    public int getLink_imagen_tiempo_actual() {
        return link_imagen_tiempo_actual;
    }

    public void setLink_imagen_tiempo_actual(int link_imagen_tiempo_actual) {
        this.link_imagen_tiempo_actual = link_imagen_tiempo_actual;
    }
}
