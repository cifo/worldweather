package com.cifo.worldweather.information;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.cifo.worldweather.R;

/**
 * Created by Alumne on 05/11/2015.
 */
public class SqliteDAO extends SQLiteOpenHelper{

    private static final String TAG ="SqliteDAO";
    private static final String DATABASE_NAME = "weatherDB";
    private static final int DATABASE_VERSION = 1;
    private static final String CIUDADES_TABLE_CREATE = "CREATE TABLE ciudades (" +
            "idbd INTEGER PRIMARY KEY AUTOINCREMENT , " +
            "idservidor INTEGER, " +
            "nombre TEXT, " +
            "sunrise TEXT, " +
            "sunset TEXT, " +
            "temperatura TEXT, " +
            "tempmax TEXT, " +
            "tempmin TEXT, " +
            "humedad TEXT, " +
            "presion TEXT, " +
            "viento TEXT, " +
            "imgciudad INTEGER, " +
            "imgtiempo INTEGER, " +
            "favorito INTEGER" +
            ");";

    private static final String CIUDADES_FORECAST = "CREATE TABLE forecast (" +
            "idbd INTEGER PRIMARY KEY AUTOINCREMENT , " +
            "idservidor INTEGER, " +
            "nombre TEXT, " +
            "timefrom TEXT, " +
            "timeto TEXT, " +
            "symbolnumber INTEGER, " +
            "symbolname TEXT, " +
            "symbolvar TEXT, " +
            "precipitationunit TEXT, " +
            "precipitationvalue REAL, " +
            "precipitationtype TEXT, " +
            "winddirectiondeg REAL, " +
            "winddirectioncode TEXT, " +
            "winddirectionname TEXT, " +
            "windspeedmps REAL, " +
            "windspeedname TEXT, " +
            "temperatureunit TEXT, " +
            "temperaturevalue REAL, " +
            "temperaturevaluemin REAL, " +
            "temperaturevaluemax REAL, " +
            "pressureunit TEXT, " +
            "pressurevalue REAL, " +
            "humidityvalue INTEGER, " +
            "humnidityunit TEXT, " +
            "cloudsvalue TEXT, " +
            "cloudsall INTEGER, " +
            "cloudsunit TEXT " +
            ");";

    private static final String DESARROLLADORES_CREATE= "CREATE TABLE desarrolladores (" +
            "avatar INTEGER PRIMARY KEY, " +
            "nombre TEXT, " +
            "apellido TEXT, " +
            "info TEXT, " +
            "correo TEXT );";

    private static final String SOCIAL_CREATE = "CREATE TABLE socialmedia (" +
            "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "id_desarrollador INTEGER, " +
            "iconoactivo INTEGER, " +
            "link TEXT, " +
            "FOREIGN KEY(id_desarrollador) REFERENCES desarrolladores(avatar));";



    public SqliteDAO(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.i(TAG, "Entra y sale de constructor OK");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(TAG, "Entra en onCreate OK");
        db.execSQL(CIUDADES_TABLE_CREATE);
        db.execSQL(CIUDADES_FORECAST);
        //crearYllenarBDCiudades(db);

        db.execSQL(DESARROLLADORES_CREATE);
        db.execSQL(SOCIAL_CREATE);

        insertData(
                db,
                R.drawable.av_juan_hidalgo,
                "Juan",    // Nombre
                "Hid", //Apellido
                "Hei !!",  // Texto
                "oprs@yahoo.es",              // Correo
                "",                                     // Facebook
                "http://www.pinterest.com/mipagina",    // Pinterest
                "http://www.google.com/mipagina",       // Google+
                "http://www.linkedin.com/mipagina",     // Linkedin
                "http://www.twitter.com/mipagina",      // Twitter
                "http://www.yahoo.com/mipagina");       // Yahoo

        insertData(
                db,
                R.drawable.av_didac_fortuny,
                "Didac",
                "Fortuny",
                "Hei !!",
                "",
                "",
                "http://www.pinterest.com/mipagina",
                "",
                "http://www.linkedin.com/mipagina",
                "http://www.twitter.com/mipagina",
                "http://www.yahoo.com/mipagina");

        insertData(
                db,
                R.drawable.av_antonio,
                "Antonio",
                "Moreno",
                "Hola q ase",
                "",
                "",
                "http://www.pinterest.com/mipagina",
                "",
                "http://www.linkedin.com/mipagina",
                "",
                "");

        // TODO copiar y pegar para los diferentes desarrolladores

        /*

        insertData(
                db,
                R.drawable.av_juan_hidalgo,             // Avatar
                "Juan",                                 // Nombre
                "Hidalgo",                              // Apellido
                "Hei !!",                               // Texto, curriculum, lo que querais.
                "online707-pres@yahoo.es",              // Correo
                "",                                     // Facebook
                "http://www.pinterest.com/mipagina",    // Pinterest
                "http://www.google.com/mipagina",       // Google+
                "http://www.linkedin.com/mipagina",     // Linkedin
                "http://www.twitter.com/mipagina",      // Twitter
                "http://www.yahoo.com/mipagina");       // Yahoo

        */
    }






    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    private static void insertData(SQLiteDatabase db,
                                   int avatar,
                                   String nombre,
                                   String apellido,
                                   String info,
                                   String correo,
                                   String facebookLink,
                                   String pinterestLink,
                                   String googleLink,
                                   String linkedinLink,
                                   String twitterLink,
                                   String yahooLink) {



        ContentValues values = new ContentValues();
        values.put("avatar", avatar);
        values.put("nombre", nombre);
        values.put("apellido", apellido);
        values.put("info", info);
        values.put("correo", correo);
        db.insert("desarrolladores", null, values);

        ContentValues valuesFacebook = new ContentValues();
        valuesFacebook.put("id_desarrollador", avatar );
        valuesFacebook.put("iconoactivo", R.drawable.ic_facebook);
        valuesFacebook.put("link", facebookLink);
        db.insert("socialmedia", null, valuesFacebook);


        ContentValues valuesPinterest = new ContentValues();
        valuesPinterest.put("id_desarrollador", avatar );
        valuesPinterest.put("iconoactivo", R.drawable.ic_pinterest);
        valuesPinterest.put("link", pinterestLink);
        db.insert("socialmedia", null, valuesPinterest);


        ContentValues valuesGoogle = new ContentValues();
        valuesGoogle.put("id_desarrollador", avatar );
        valuesGoogle.put("iconoactivo", R.drawable.ic_googleplus);
        valuesGoogle.put("link", googleLink);
        db.insert("socialmedia", null, valuesGoogle);


        ContentValues valuesLinkedin = new ContentValues();
        valuesLinkedin.put("id_desarrollador", avatar );
        valuesLinkedin.put("iconoactivo", R.drawable.ic_linkedin);
        valuesLinkedin.put("link", linkedinLink);
        db.insert("socialmedia", null, valuesLinkedin);

        ContentValues valuesTwitter = new ContentValues();
        valuesTwitter.put("id_desarrollador", avatar );
        valuesTwitter.put("iconoactivo", R.drawable.ic_twitter);
        valuesTwitter.put("link", twitterLink);
        db.insert("socialmedia", null, valuesTwitter);

        ContentValues valuesYahoo = new ContentValues();
        valuesYahoo.put("id_desarrollador", avatar );
        valuesYahoo.put("iconoactivo", R.drawable.ic_yahoo);
        valuesYahoo.put("link", yahooLink);
        db.insert("socialmedia", null, valuesYahoo);

        Log.i(TAG, "Insertados Desarrolladores OK");
    }
}
