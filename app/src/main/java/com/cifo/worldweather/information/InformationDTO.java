package com.cifo.worldweather.information;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.cifo.worldweather.Desarrollador;
import com.cifo.worldweather.R;
import com.cifo.worldweather.SocialMedia;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Created by Alumne on 28/10/2015.
 */
public class InformationDTO {

    private static SqliteDAO dao;

    private static ArrayList<Ciudad> ciudades_por_defecto = new ArrayList<Ciudad>();
    private static ArrayList<Ciudad> ciudades_favoritas = new ArrayList<Ciudad>();
    private static ArrayList<Desarrollador> desarrolladores = new ArrayList<Desarrollador>();

    // Constructor, vinculo con el DAO
    public InformationDTO(Context context) {
        dao = new SqliteDAO(context);
    }


    public static ArrayList<Ciudad> getCiudades_por_defecto() {
        return InformationDTO.ciudades_por_defecto;
    }

    public static Ciudad getCiudades_por_defecto(int i){
        return InformationDTO.ciudades_por_defecto.get(i);
    }

    public static void setCiudades_por_defecto(ArrayList<Ciudad> ciudades_por_defecto) {
        InformationDTO.ciudades_por_defecto = ciudades_por_defecto;
    }

    public static void setCiudades_por_defecto(Ciudad ciudad_por_defecto){
        InformationDTO.ciudades_por_defecto.add(ciudad_por_defecto);
    }

    public static ArrayList<Ciudad> getCiudades_favoritas() {
        return InformationDTO.ciudades_favoritas;
    }

    public static Ciudad getCiudades_favoritas(int i){
        return InformationDTO.ciudades_favoritas.get(i);
    }

    public static void setCiudades_favoritas(ArrayList<Ciudad> ciudades_favoritas) {
        InformationDTO.ciudades_favoritas = ciudades_favoritas;
    }

    public static void setCiudades_favoritas(Ciudad ciudad_favorita){
        InformationDTO.ciudades_favoritas.add(ciudad_favorita);
    }

    public static void quitarCiudades_favoritas(Ciudad ciudad_favorita){
        InformationDTO.ciudades_favoritas.remove(ciudad_favorita);
    }




    public static void cargarCiudades_por_defecto(){

        // METEMOS LAS CIUDADES POR DEFECTO.
        // TODO: AÃ±adir la informacion que falta a cada ciudad.
        Ciudad ciudad_paris = new Ciudad("Paris");
        ciudad_paris.setCiudad_idbd(2988507);
        ciudad_paris.setCiudad_temperatura("24Âº");
        ciudad_paris.setLink_imagen_tiempo_actual(R.drawable.spinner_despejado_dia);
        setCiudades_por_defecto(ciudad_paris);

        Ciudad ciudad_roma = new Ciudad("Roma");
        ciudad_roma.setCiudad_temperatura("16Âº");
        ciudad_roma.setLink_imagen_tiempo_actual(R.drawable.spinner_despejado_noche);
        setCiudades_por_defecto(ciudad_roma);

        Ciudad ciudad_singapur = new Ciudad("Singapur");
        ciudad_singapur.setCiudad_temperatura("8Âº");
        ciudad_singapur.setLink_imagen_tiempo_actual(R.drawable.spinner_lluvia_dia);
        setCiudades_por_defecto(ciudad_singapur);

        Ciudad ciudad_newyork = new Ciudad("Nueva York");
        ciudad_newyork.setCiudad_idbd(5128638);
        ciudad_newyork.setCiudad_temperatura("23Âº");
        ciudad_newyork.setLink_imagen_tiempo_actual(R.drawable.spinner_lluvia_noche);
        setCiudades_por_defecto(ciudad_newyork);

        Ciudad ciudad_riojaneiro = new Ciudad("Rio de Janeiro");
        ciudad_riojaneiro.setCiudad_temperatura("31Âº");
        ciudad_riojaneiro.setLink_imagen_tiempo_actual(R.drawable.spinner_nieve_dia);
        setCiudades_por_defecto(ciudad_riojaneiro);

        Ciudad ciudad_sydney = new Ciudad("Sydney");
        ciudad_sydney.setCiudad_temperatura("28Âº");
        ciudad_sydney.setLink_imagen_tiempo_actual(R.drawable.spinner_nieve_noche);
        ciudad_sydney.setCiudad_idbd(2147714);
        setCiudades_por_defecto(ciudad_sydney);

        Ciudad ciudad_ciudadcabo = new Ciudad("Ciudad del Cabo");
        ciudad_ciudadcabo.setCiudad_temperatura("4Âº");
        ciudad_ciudadcabo.setLink_imagen_tiempo_actual(R.drawable.spinner_nubes_dia);
        setCiudades_por_defecto(ciudad_ciudadcabo);

        Ciudad ciudad_elcairo = new Ciudad("El Cairo");
        ciudad_elcairo.setCiudad_temperatura("41Âº");
        ciudad_elcairo.setLink_imagen_tiempo_actual(R.drawable.spinner_nubes_noche);
        setCiudades_por_defecto(ciudad_elcairo);

        Ciudad ciudad_londres = new Ciudad("Londres");
        ciudad_londres.setCiudad_temperatura("6Âº");
        ciudad_londres.setLink_imagen_tiempo_actual(R.drawable.spinner_lluvia_dia);
        setCiudades_por_defecto(ciudad_londres);

        Ciudad ciudad_moscu = new Ciudad("Moscu");
        ciudad_moscu.setCiudad_temperatura("-8Âº");
        ciudad_moscu.setLink_imagen_tiempo_actual(R.drawable.spinner_despejado_dia);
        setCiudades_por_defecto(ciudad_moscu);
    }


    private Ciudad getCiudadFromDatabase(int indexServidor){

        Ciudad ciudad = new Ciudad();
        SQLiteDatabase db = dao.getReadableDatabase();
        Cursor cs = db.rawQuery("SELECT * FROM ciudades WHERE idservidor = ?", new String[]{String.valueOf(indexServidor)});

        if (cs.moveToFirst()) {
            ciudad.setCiudad_idbd(cs.getInt(0));
            ciudad.setCiudad_idservidor(cs.getInt(1));
            ciudad.setCiudad_sunrise(null);
            ciudad.setCiudad_sunset(null);
            //ciudad.setCiudad_sunrise(cs.getType(2));
            //ciudad.setCiudad_sunset(cs.getType(3));
            ciudad.setCiudad_temperatura(cs.getString(4));
            ciudad.setCiudad_temperatura_maxima(cs.getString(5));
            ciudad.setCiudad_temperatura_minima(cs.getString(6));
            ciudad.setCiudad_humedad(cs.getString(7));
            ciudad.setCiudad_presion_atmosferica(cs.getString(8));
            ciudad.setCiudad_viento(cs.getString(9));
            ciudad.setLink_imagen_ciudad(cs.getInt(10));
            ciudad.setLink_imagen_tiempo_actual(cs.getInt(11));
            ciudad.setCiudad_favorito(cs.getInt(12));
        }

        db.close();
        return ciudad;
    }

    private void crearYllenarBDCiudades(SQLiteDatabase db) {

        dao.getWritableDatabase();
        ContentValues values = new ContentValues();

        for (int i = 0; i < ciudades_por_defecto.size(); i++) {
            values.put("idservidor", ciudades_por_defecto.get(i).getCiudad_idservidor());
            values.put("nombre", ciudades_por_defecto.get(i).getCiudad_nombre());
            values.put("temperatura", ciudades_por_defecto.get(i).getCiudad_temperatura());
            values.put("tempmax", ciudades_por_defecto.get(i).getCiudad_temperatura_maxima());
            values.put("tempmin", ciudades_por_defecto.get(i).getCiudad_temperatura_minima());
            values.put("humedad", ciudades_por_defecto.get(i).getCiudad_humedad());
            values.put("presion", ciudades_por_defecto.get(i).getCiudad_presion_atmosferica());
            values.put("viento", ciudades_por_defecto.get(i).getCiudad_viento());
            values.put("imgciudad", ciudades_por_defecto.get(i).getLink_imagen_ciudad());
            values.put("imgtiempo", ciudades_por_defecto.get(i).getLink_imagen_tiempo_actual());
            values.put("favorito", 0);
            db.insert("ciudades", null, values);
        }
        dao.close();
    }



    private void updateCiudadToDatabase(Ciudad ciudad){
        String[] datos = new String[]{
                /*ciudad.getCiudad_sunrise(),
                ciudad.getCiudad_sunset(),*/
                ciudad.getCiudad_temperatura(),
                ciudad.getCiudad_temperatura_maxima(),
                ciudad.getCiudad_temperatura_minima(),
                ciudad.getCiudad_humedad(),
                ciudad.getCiudad_presion_atmosferica(),
                ciudad.getCiudad_viento(),
                String.valueOf(ciudad.getLink_imagen_ciudad()),
                String.valueOf(ciudad.getLink_imagen_tiempo_actual()),
                String.valueOf(ciudad.getCiudad_favorito()),
                String.valueOf(ciudad.getCiudad_idservidor())
        };


        SQLiteDatabase db = dao.getWritableDatabase();

        db.execSQL("UPDATE ciudades SET " +
                /*"sunrise = ?, " +
                "sunset = ?,  " +*/
                "temperatura = ?, " +
                "tempmax = ?, " +
                "tempmin = ?, " +
                "humedad = ?, " +
                "presion = ?" +
                "viento = ?" +
                "imgciudad = ?" +
                "imgtiempo = ?" +
                "favorito = ?" +
                "WHERE idservidor = ?;", datos);
    }



    // DESARROLLADORES - NO TRASPASING BELOW THIS POINT :) !!
    // Conecta a bd y obtiene todos los desarrolladores
    // en una lista ordenada aleatoriamente
    public static ArrayList<Desarrollador> getAllDesarrolladores() {

        ArrayList<Desarrollador> desarrolladorList = new ArrayList<>();

        SQLiteDatabase db = dao.getReadableDatabase();

        Cursor cs = db.rawQuery("SELECT * FROM desarrolladores", null);

        if(cs.moveToFirst()) {
            do
            {
                int avatar = cs.getInt(0);
                String nombre = cs.getString(1);
                String apellido = cs.getString(2);
                String info = cs.getString(3);
                String correo = cs.getString(4);

                ArrayList<SocialMedia> listaSocial = new ArrayList<SocialMedia>();

                Cursor cursorsocial = db.rawQuery("SELECT iconoactivo, link FROM socialmedia WHERE id_desarrollador=?", new String[] { String.valueOf(avatar)});
                if(cursorsocial.moveToFirst()) {
                    do
                    {
                        int iconoactivo = cursorsocial.getInt(0);
                        String link = cursorsocial.getString(1);
                        listaSocial.add(new SocialMedia(iconoactivo, link));
                    }
                    while(cursorsocial.moveToNext());

                }
                desarrolladorList.add(new Desarrollador(avatar, nombre, apellido, info, correo, listaSocial));

            }
            while(cs.moveToNext());


        }

        // shuffle the list (ordenarla aleatoriamente)
        long seed = System.nanoTime();
        Collections.shuffle(desarrolladorList, new Random(seed));

        return desarrolladorList;
    }


    //Select de la base de datos pasando el nombre de la ciudad.
    //Recibido de FragmentSpinner

    public Ciudad getCiudad(String nombreCiudad) {

        Ciudad ciudad = new Ciudad();
        SQLiteDatabase db = dao.getReadableDatabase();
        Cursor cs = db.rawQuery("SELECT * FROM ciudades WHERE nombre = ?", new String[]{nombreCiudad});

        if (cs.moveToFirst()) {
            ciudad.setCiudad_idbd(cs.getInt(0));
            ciudad.setCiudad_idservidor(cs.getInt(1));
            ciudad.setCiudad_temperatura(cs.getString(2));
            ciudad.setCiudad_temperatura_maxima(cs.getString(3));
            ciudad.setCiudad_temperatura_minima(cs.getString(4));
            ciudad.setCiudad_humedad(cs.getString(5));
            ciudad.setCiudad_presion_atmosferica(cs.getString(6));
            ciudad.setCiudad_viento(cs.getString(7));
            ciudad.setLink_imagen_ciudad(cs.getInt(8));
            ciudad.setLink_imagen_tiempo_actual(cs.getInt(9));
            ciudad.setCiudad_favorito(cs.getInt(10));
        }



        db.close();

        return ciudad;
    }






}