package com.cifo.worldweather.information;

/**
 * Created by Alumne on 27/10/2015.
 */
public class Ciudad_horas {

    // Las horas vendran marcados por +3, +6, +9...
    private int hora;
    private String simbolo_tiempo;
    private int temperatura;

    public Ciudad_horas(int hora, String simbolo_tiempo, int temperatura) {
        this.hora = hora;
        this.simbolo_tiempo = simbolo_tiempo;
        this.temperatura = temperatura;
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

    public String getSimbolo_tiempo() {
        return simbolo_tiempo;
    }

    public void setSimbolo_tiempo(String simbolo_tiempo) {
        this.simbolo_tiempo = simbolo_tiempo;
    }

    public int getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(int temperatura) {
        this.temperatura = temperatura;
    }
}
