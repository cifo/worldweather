package com.cifo.worldweather;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.cifo.worldweather.favoritos.Favoritas;
import com.cifo.worldweather.information.Ciudad;
import com.cifo.worldweather.information.InformationDTO;

public class Inicio extends AppCompatActivity {

    //View v;
    public static final String PREFERENCE_FILE="archivo_preferencias";
    public static final String PREFERENCE_KEY_DEFAULT = "ID_CIUDAD";
    public static final String  PREFERENCE_KEY_DEFAULT_NAME = "NOMBRE_CIUDAD";

    Ciudad ciudadDefecto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pantalla_principal);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        SharedPreferences spCiudadInicio = getSharedPreferences(PREFERENCE_FILE, Inicio.MODE_PRIVATE);
        int ciudadInicio = spCiudadInicio.getInt(PREFERENCE_KEY_DEFAULT, 0);
        String ciudadNombre = spCiudadInicio.getString(PREFERENCE_KEY_DEFAULT_NAME, "Sin_dato");

        //Log.d("ciudadInicio, ",String.valueOf(ciudadInicio));
        Log.d("ciudadInicio, ", ciudadNombre);

       /* TextView tx = (TextView)findViewById(R.id.textView2);
        TextView tx1 = (TextView)findViewById(R.id.textView3);
        TextView tx2 = (TextView)findViewById(R.id.textView7);
        TextView tx3 = (TextView)findViewById(R.id.textView9);
        TextView tx4 = (TextView)findViewById(R.id.textView11);
        TextView tx5 = (TextView)findViewById(R.id.textView13);

        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/lanenar.ttf");


        tx.setTypeface(custom_font);
        tx1.setTypeface(custom_font);
        tx2.setTypeface(custom_font);
        tx3.setTypeface(custom_font);
        tx4.setTypeface(custom_font);
        tx5.setTypeface(custom_font);*/


        if (!ciudadNombre.equals("Sin_dato")) {
            InformationDTO dto = new InformationDTO(this);

            ciudadDefecto = dto.getCiudad(ciudadNombre);

        }








        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                startActivity(new Intent(Inicio.this, Favoritas.class));
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_inicio, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        /*
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
        */

        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.action_seleccion_ciudad:
                // Snackbar.make(v, "HAS PULSADO Ciudad", Snackbar.LENGTH_LONG).show();
                Intent ir = new Intent(Inicio.this, ListaCiudades_DetalleCiudad_Activity.class);
                startActivity(ir);
                break;
            case R.id.action_mapa:
                //Snackbar.make(v, "HAS PULSADO Mapa", Snackbar.LENGTH_LONG).show();
                break;

        }
        return true;
    }


}
