package com.cifo.worldweather;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

public class ListaCiudades_DetalleCiudad_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_ciudades_detalle_ciudad);


        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        FragmentListaCiudades fragment = new FragmentListaCiudades();
        ft.replace(R.id.contentFragment, fragment);

        ft.commit();
    }
}
