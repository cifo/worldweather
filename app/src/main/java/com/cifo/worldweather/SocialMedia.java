package com.cifo.worldweather;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by Alumne on 23/10/2015.
 */
public class SocialMedia implements Parcelable {

    // Class members
    private int iconoActivo;
    private String link;

    // Constructor
    public SocialMedia(int iconoActivo, String link) {
        this.iconoActivo = iconoActivo;
        this.link = link;
    }

    // Setters
    public void setLink(String link) {
        this.link = link;
    }

    // Getters
    public int getIconoActivo() {
        return iconoActivo;
    }

    public String getLink() {
        return link;
    }

    // Parcelable methods
    protected SocialMedia(Parcel in) {
        iconoActivo = in.readInt();
        link = in.readString();
    }

    public static final Parcelable.Creator<SocialMedia> CREATOR = new Parcelable.Creator<SocialMedia>() {
        @Override
        public SocialMedia createFromParcel(Parcel in) {
            return new SocialMedia(in);
        }

        @Override
        public SocialMedia[] newArray(int size) {
            return new SocialMedia[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(iconoActivo);
        dest.writeString(link);
    }

}
