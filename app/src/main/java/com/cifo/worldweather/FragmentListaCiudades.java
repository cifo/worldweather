package com.cifo.worldweather;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cifo.worldweather.information.Ciudad;
import com.cifo.worldweather.utils.AdaptadorRecycler;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link .} interface
 * to handle interaction events.
 * Use the {@link FragmentListaCiudades#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentListaCiudades extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public ArrayList<Ciudad> listaCiudades;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    // private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentListaCiudades.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentListaCiudades newInstance(String param1, String param2) {
        FragmentListaCiudades fragment = new FragmentListaCiudades();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentListaCiudades() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        insertarCiudad();

        View v = inflater.inflate(R.layout.fragment_lista_ciudades, container, false);

        RecyclerView recycler = (RecyclerView) v.findViewById(R.id.fragmentListaCiudades_RVCiudades);
        recycler.setHasFixedSize(true);

        AdaptadorRecycler adaptador = new AdaptadorRecycler(listaCiudades, R.layout.item_ciudad);

        recycler.setAdapter(adaptador);
        recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        recycler.setItemAnimator(new DefaultItemAnimator());

        return v;
    }

    private void insertarCiudad() {

        listaCiudades = new ArrayList<>();

        Ciudad singapur = new Ciudad(R.drawable.imgsng, "Singapur", "22º", R.drawable.sol);
        listaCiudades.add(singapur);

        Ciudad barcelona = new Ciudad(R.drawable.imgsng, "Barcelona", "12º", R.drawable.sol);
        listaCiudades.add(barcelona);

        Ciudad rio = new Ciudad(R.drawable.riojaneiro, "Rio de Janeiro", "35º", R.drawable.sol);
        listaCiudades.add(rio);

        Ciudad capetown = new Ciudad(R.drawable.riojaneiro, "Cape Town", "35º", R.drawable.sol);
        listaCiudades.add(capetown);

        Ciudad roma = new Ciudad(R.drawable.riojaneiro, "Roma", "35º", R.drawable.sol);
        listaCiudades.add(roma);

        Ciudad paris = new Ciudad(R.drawable.riojaneiro, "París", "35º", R.drawable.sol);
        listaCiudades.add(paris);

        Ciudad sidney = new Ciudad(R.drawable.riojaneiro, "Sidney", "35º", R.drawable.sol);
        listaCiudades.add(sidney);

        Ciudad cairo = new Ciudad(R.drawable.riojaneiro, "El Cairo", "35º", R.drawable.sol);
        listaCiudades.add(cairo);

        Ciudad moscu = new Ciudad(R.drawable.riojaneiro, "Moscú", "35º", R.drawable.sol);
        listaCiudades.add(moscu);

        Ciudad ny = new Ciudad(R.drawable.riojaneiro, "Nueva York", "35º", R.drawable.sol);
        listaCiudades.add(ny);

    }

    // TODO: Rename method, update argument and hook method into UI event
/*    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }*/

/*    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }*/

/*    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */


/*    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }*/

}