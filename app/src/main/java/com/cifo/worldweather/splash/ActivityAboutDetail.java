package com.cifo.worldweather.splash;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.cifo.worldweather.Desarrollador;
import com.cifo.worldweather.R;
import com.cifo.worldweather.SocialMedia;
import com.cifo.worldweather.information.Ciudad;
import com.cifo.worldweather.utils.OpenWeatherApiOLD;
import com.cifo.worldweather.utils.WeatherApi;

import java.io.IOException;

public class ActivityAboutDetail extends AppCompatActivity {

    private static final String TITULO_MENSAJE= "Contacto desde App WorldWeather";
    private static final String MENSAJE= "Hola ";
    private ImageView avatar;
    private TextView info, name;
    private Button mailBtn;
    private ImageView fb,tw, lk,pt,gg,yh;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_detail);

        // OBTENER REFERENCIAS LAYOUT Y DATOS DEL DESARROLLADOR
        initComponents();
        Desarrollador des = getIntent().getExtras().getParcelable("desarrollador");

        // GESTIONAR DATOS
        avatar.setImageResource(des.getAvatar());
        info.setText(des.getInfo());
        name.setText(des.getNombreCompleto());

        // GESTIONAR BOTON MAIL
        String mail = des.getMail();
        if(!mail.equals("")) {
            mailBtn.setBackgroundResource(des.getMailLinkActiveResource());
            String mailLink = "mailto:" + mail + "?subject=" + TITULO_MENSAJE + "&body=" + MENSAJE + des.getNombre();
            final Uri uri = Uri.parse(mailLink);
            mailBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }
            });
        }else{
            mailBtn.setVisibility(View.INVISIBLE);
        }

        // GESTIONAR ICONOS SOCIALMEDIA
        int count = 0;
        ImageView socialImg ;
        for( SocialMedia social : des.getSocialList()) {
            final String link = social.getLink();
            switch(count) {
                case 0:
                    socialImg = fb;
                    break;
                case 1:
                    socialImg = tw;
                    break;
                case 2:
                    socialImg = lk;
                    break;
                case 3:
                    socialImg = pt;
                    break;
                case 4:
                    socialImg = gg;
                    break;
                case 5:
                    socialImg = yh;
                    break;
                default:
                    socialImg = fb;
            }

            if(!link.equals("")) {
                // OBTENER ICONO ACTIVO
                socialImg.setImageDrawable(getResources().getDrawable(social.getIconoActivo()));
                // ASOCIAR EL LISTENER
                socialImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Uri uri = Uri.parse(link);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }
                });
            }else{
                // DESOCUPAR ESPACIO
                socialImg.setVisibility(View.GONE);
            }

            count = count + 1;
        }
    }


    private void initComponents() {
        avatar = (ImageView) findViewById(R.id.activity_about_detail_avatar);
        info = (TextView) findViewById(R.id.activity_about_detail_info);
        name = (TextView) findViewById(R.id.activity_about_detail_name);
        mailBtn = (Button) findViewById(R.id.fragment_about_mail);
        fb = (ImageView) findViewById(R.id.imgViewFace);
        tw = (ImageView) findViewById(R.id.imgViewTwit);
        lk = (ImageView) findViewById(R.id.imgViewLinke);
        pt = (ImageView) findViewById(R.id.imgViewPinterest);
        gg = (ImageView) findViewById(R.id.imgViewGooglePlus);
        yh = (ImageView) findViewById(R.id.imgViewYahoo);


        // TODO Prueba de llamada a la clase para obtener datos
        /*OpenWeatherApiOLD api = new OpenWeatherApiOLD();
        api.getWeather("Barcelona,es");
        api.getWeather("London,uk");
*/


        WeatherApi api = new WeatherApi();
        try {
            api.getWeather("Barcelona,es");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
