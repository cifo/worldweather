package com.cifo.worldweather.splash;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.cifo.worldweather.Desarrollador;
import com.cifo.worldweather.R;
import com.cifo.worldweather.information.Ciudad;

public class SplashImage extends AppCompatActivity implements ListViewInterface {

    private ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);




        setContentView(R.layout.activity_splash_image);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //1 CONSTRUIR LOS LAYOUTS EN XML

        //2 GENERAR LOS FRAGMENTS A UTILIZAR

        //3 DEBEREMO EN XML DE LA ACTIVITY GENERAR EL WIDGET DEL VIEWPAGER

        //4 REFERENCIAR EL VIEW PAGER
        pager = (ViewPager) findViewById(R.id.pager);

        //5 INSTANCIAR EL ADAPTADOR DEL VIEW PAGER

        FragmentAdapterSplash adapter = new FragmentAdapterSplash(getSupportFragmentManager());

        pager.setAdapter(adapter);



    }

    // Implementacion de la interface ListViewInterface
    @Override
    public void onDesarrolladorSelected(Desarrollador desarrollador) {

        // Abrimos otra activity

        Intent intent = new Intent(this, ActivityAboutDetail.class);
        intent.putExtra("desarrollador", desarrollador);
        startActivity(intent);


    }

    @Override
    public void onCiudadSelected(Ciudad c) {

    }

    private class FragmentAdapterSplash extends FragmentStatePagerAdapter {
        public FragmentAdapterSplash(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new FragmentVideoView();
                case 1:
                    return new FragmentAbout();
                case 2:
                    return new FragmentSpinner();


            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }


}
