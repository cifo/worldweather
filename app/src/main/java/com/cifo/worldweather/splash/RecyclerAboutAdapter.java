package com.cifo.worldweather.splash;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.cifo.worldweather.Desarrollador;
import com.cifo.worldweather.R;

import java.util.ArrayList;

/**
 * Created by Alumne on 30/10/2015.
 */

//Adapter

public class RecyclerAboutAdapter extends RecyclerView.Adapter<RecyclerAboutAdapter.ViewHolder>  {
    public final ArrayList<Desarrollador> items;
    private final int itemLayout;
    OnItemClickListener mItemClickListener;
    private ListViewInterface mListener;
    Desarrollador item;

    //Constructor que pide lista de datos y layout donde represantar cada dato
    public RecyclerAboutAdapter(ArrayList<Desarrollador> items, int itemLayout) {
        this.items = items;
        this.itemLayout = itemLayout;
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater. from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ViewHolder(v);
    }
    @Override
    public void onBindViewHolder(ViewHolder holder,final int position) {
        item = items.get(position);
        final Desarrollador d = new Desarrollador("Federico");
        holder.image.setImageResource(item.getAvatar());
        holder.nombre.setText(item.getNombre());
        holder.apellido.setText(item.getApellido());
        /*
        holder.btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("EEEEE", d.getNombre());
                mListener.onDesarrolladorSelected(d);
            }
        });
*/
    }




    @Override
    public int getItemCount() {
        return items.size();
    }




    // ViewHolder

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView image;
        public TextView nombre, apellido;
        public ImageButton btn;
        public ViewHolder(View itemView) {
            super(itemView);
            // Referencias a los widgets del item de la lista
            image = (ImageView) itemView.findViewById(R.id.itemAbout_imgAvatar);
            nombre = (TextView) itemView.findViewById(R.id.itemAbout_txtNombre);
            apellido = (TextView) itemView.findViewById(R.id.itemAbout_txtApellido);
            //btn = (ImageButton) itemView.findViewById(R.id.itemAbout_btnInfo);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {

            if (mItemClickListener != null) {
               mItemClickListener.onItemClick(v, getPosition());
            }
        }
    }

    public interface OnItemClickListener {
        public void onItemClick(View view , int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener =  mItemClickListener;
    }

}