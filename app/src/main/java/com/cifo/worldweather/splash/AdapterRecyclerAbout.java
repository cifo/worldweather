package com.cifo.worldweather.splash;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cifo.worldweather.Desarrollador;
import com.cifo.worldweather.R;

import java.util.ArrayList;

/**
 * Created by juan on 30/10/15.
 */
public class AdapterRecyclerAbout extends RecyclerView.Adapter<AdapterRecyclerAbout.ViewHolder>  {

    // lista de datos del tipo Desarrollador
    public final ArrayList<Desarrollador> desarrolladorList;

    // referencia del layout que representara cada elemento
    private final int itemLayout;

    // inner interface member, usada para captar el click en el item
    protected AdapterRecyclerAbout.OnItemClickInterface mItemClickListener;

    // Constructor recibe lista con los desarrolladores
    // y referencia al layout donde representar cada desarrollador
    public AdapterRecyclerAbout(ArrayList<Desarrollador> desarrolladorList, int itemLayout) {
        this.desarrolladorList = desarrolladorList;
        this.itemLayout = itemLayout;
    }


    // Se infla el layout
    @Override
    public AdapterRecyclerAbout.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate( itemLayout, parent, false);
        return new ViewHolder(v);
    }

    // Aqui se asignan los datos a los widgets del layout
    @Override
    public void onBindViewHolder(AdapterRecyclerAbout.ViewHolder holder, int position) {
        // Obtengo los datos de cada elemento de la lista proporcionada en el constructor
        final Desarrollador item = desarrolladorList.get(position);
        // Los asigno a donde se representaran
        holder.avatar.setImageResource(item.getAvatar());
        holder.nombre.setText(item.getNombre());
        holder.apellido.setText(item.getApellido());

        // holder.itemView.setTag(item);
    }

    // Metodo que debe devolver el total de elementos del array de datos.
    @Override
    public int getItemCount() {
        return desarrolladorList.size();
    }

    // ViewHolder que implementa el OnClickListener

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView avatar;
        public TextView nombre, apellido;

        // Constructor recibe la vista layout de un item
        // Aqui se captan los widgets del layout que representaran los datos
        public ViewHolder(View itemView) {
            super(itemView);
            // Referencias a los widgets del layout
            avatar = (ImageView) itemView.findViewById(R.id.itemAbout_imgAvatar);
            nombre = (TextView) itemView.findViewById(R.id.itemAbout_txtNombre);
            apellido = (TextView) itemView.findViewById(R.id.itemAbout_txtApellido);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            // Cada vez q se hace click en un item del recyclerview
            // Ejecuto el metodo onItemClick de la interface
            // Le paso la vista y la posicion de esta en el recyclerview.
            mItemClickListener.onItemClick(v, getPosition());
        }
    }


    // Recibe una instancia de la interface OnItemClickInterface
    // Lo asigna al miembro de clase mItemClickListener
    public void SetOnItemClickListener(OnItemClickInterface onItemClickListener) {
        this.mItemClickListener = onItemClickListener;
    }

    // Interface que se usa para captar la posicion pulsada en el RecyclerView
    public interface OnItemClickInterface {
        // Recibe una vista y su posicion en el recyclerview.
        void onItemClick(View v, int position);
    }

}
