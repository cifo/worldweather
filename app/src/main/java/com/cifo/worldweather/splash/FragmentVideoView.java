package com.cifo.worldweather.splash;


import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;

import com.cifo.worldweather.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentVideoView extends Fragment {

    VideoView myVideoView;

    public FragmentVideoView() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Video en Splash
        View v = inflater.inflate(R.layout.fragment_splash_video, container, false);
        VideoView videoView = (VideoView) v.findViewById(R.id.splash_videoView);

        // Cambiar el nombre del video
        Uri path = Uri.parse("android.resource://com.cifo.worldweather/"
                + R.raw.sidney360);

        // videoView.setRotation(90f);

        videoView.setVideoURI(path);
        videoView.start();

        //TODO: IMPLEMENTACION DEL VIDEO AUTOPLAY Y LOOP.

        return v;
    }


}
