package com.cifo.worldweather.splash;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.cifo.worldweather.Desarrollador;
import com.cifo.worldweather.R;
import com.cifo.worldweather.information.InformationDTO;
import java.util.ArrayList;



/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentAbout#} factory method to
 * create an instance of this fragment.
 */
public class FragmentAbout extends Fragment {

    private ArrayList<Desarrollador> desarrolladorList;
    private ListViewInterface mListener;


    public FragmentAbout() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (ListViewInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_about, container, false);

        // Obtengo los datos de la DB.
        InformationDTO db = new InformationDTO(getActivity());
        desarrolladorList = db.getAllDesarrolladores();

        // Cojo referencia al layout xml donde tenemos nuestro recyclerview
        RecyclerView recyclerView = (RecyclerView) v.findViewById(R.id.fragmentAbout_list);
        recyclerView.setHasFixedSize(true);

        // Le seteo nuestra clase adaptadora pasandole una nueva instancia
        // con la lista de datos y el layout donde se representara cada dato.
        AdapterRecyclerAbout adapter = new AdapterRecyclerAbout(desarrolladorList, R.layout.item_about);
        recyclerView.setAdapter(adapter);

        // Le seteo el tipo de lista que quiero (Linear, Grid, Staggered)
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        // Llamo al metodo SetOnItemClickListener definido en la clase del adapter
        // Le paso una instancia de su interface con su metodo implementado
        // en el cual se llama a su vez al metodo de la otra interface que comunica
        // los datos a la activity que lo engloba, SplashImage.
        adapter.SetOnItemClickListener(new AdapterRecyclerAbout.OnItemClickInterface() {
            @Override
            public void onItemClick(View v, int position) {
                // Obtener detalle del desarrollador
                Desarrollador d = desarrolladorList.get(position);
                // Llamar al metodo de la interface ListViewInterface
                mListener.onDesarrolladorSelected(d);

            }
        });

        return v;
    }



}
