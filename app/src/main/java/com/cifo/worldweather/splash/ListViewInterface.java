package com.cifo.worldweather.splash;

import com.cifo.worldweather.Desarrollador;
import com.cifo.worldweather.information.Ciudad;


/**
 * Created by Alumne on 28/10/2015.
 */
public interface ListViewInterface {

    public void onDesarrolladorSelected(Desarrollador d);

    public void onCiudadSelected(Ciudad c);

}
