package com.cifo.worldweather.splash;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import com.cifo.worldweather.Inicio;
import com.cifo.worldweather.R;
import com.cifo.worldweather.information.Ciudad;
import com.cifo.worldweather.information.InformationDTO;

import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Toast;

import java.util.ArrayList;

public class FragmentSpinner extends Fragment  implements OnItemSelectedListener {

    private static final String TAG = "FRAGMENT SPINNER";
    private View frag;
    private ImageView imagenSplash;
    private Spinner spinner;
    ArrayList<Ciudad> listObjCiudad;
    ArrayList<String> listNombresCiudad= new ArrayList<String>();
    String nombreCiudadSeleccionada;


    public FragmentSpinner() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        frag = inflater.inflate(R.layout.fragment_splash_spinner, container, false);
        imagenSplash= (ImageView) frag.findViewById(R.id.splashSeleccionarCiudad_imageView_ciudad);
        spinner = (Spinner) frag.findViewById(R.id.splashSeleccionarCiudad_imageView_spinner_ciudad);
        addItemsOnSpinner();
        spinner.setOnItemSelectedListener(this);





        //Log.e(TAG, "recollit spinner de la vista , numero de items :" + spinner.getChildCount());
      /* *
         * no funciona
         * http://www.tutorialspoint.com/android/android_spinner_control.htm
         * */





        Button boton = (Button) frag.findViewById(R.id.splashSeleccionarCiudad_btn_select_spinner_val);
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: Guardar Ciudad en los favoritos del usuario.

                //Spinner spinner = (Spinner) frag.findViewById(R.id.splashSeleccionarCiudad_imageView_spinner_ciudad);
                int seleccionada = spinner.getSelectedItemPosition();

                // Create an ArrayAdapter using the string array and a default spinner layout
                SharedPreferences sp= getActivity().getSharedPreferences(Inicio.PREFERENCE_FILE, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor  = sp.edit();
                editor.putInt(Inicio.PREFERENCE_KEY_DEFAULT, seleccionada);
                editor.putString(Inicio.PREFERENCE_KEY_DEFAULT_NAME, nombreCiudadSeleccionada);
                editor.commit();


                //InformationDTO.setCiudades_favoritas(InformationDTO.getCiudades_por_defecto(seleccionada));

                Intent intent = new Intent(getContext(), Inicio.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                getActivity().finish();
            }
        });

        //TODO: AQUI DEBEMOS GESTIONAR EL SPINNER

        return frag;
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


        nombreCiudadSeleccionada = parent.getItemAtPosition(position).toString();
        Toast.makeText(parent.getContext(),
                "Selected : " + parent.getItemAtPosition(position).toString(),
                Toast.LENGTH_SHORT).show();
        changeImageSpinner(listObjCiudad.get(position));
        Log.e(TAG, "item seleccionat ");
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    public void addItemsOnSpinner() {
        //Log.e(TAG, "s'entra a addItemsOnSpinner()");
        InformationDTO.cargarCiudades_por_defecto();
        listObjCiudad = InformationDTO.getCiudades_por_defecto();


        for (int j = 0; j< listObjCiudad.size(); j++){
            listNombresCiudad.add(listObjCiudad.get(j).getCiudad_nombre());
        }
        //Log.e(TAG, "es procedeix a afegir ciutats");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, listNombresCiudad);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);


        /*ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(dataAdapter);*/
    }
    public void changeImageSpinner(Ciudad ciudadSeleccionada){
        //frag.findViewById(R.id.splashSeleccionarCiudad_imageView_ciudad).setBackground(itemAtPosition.getLink_imagen_ciudad());
//        frag.findViewById(R.id.splashSeleccionarCiudad_imageView_ciudad).src(itemAtPosition.getLink_imagen_ciudad());
        imagenSplash.setImageResource(ciudadSeleccionada.getLink_imagen_ciudad());

    }


}
