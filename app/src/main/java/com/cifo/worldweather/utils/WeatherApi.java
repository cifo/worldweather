package com.cifo.worldweather.utils;

import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.cifo.worldweather.R;
import com.cifo.worldweather.information.Ciudad;
import com.cifo.worldweather.information.SqliteDAO;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Alumne on 10/11/2015.
 */
public class WeatherApi {

    Ciudad miCiudad;

    // key necesaria para obtener datos
    private static final String APIKEY = "1c469181d318bc30f57d87f58af99b2f";

    // Recibe String del tipo "Barcelona,es"
    private static String NOW_ENDPOINT =
            "http://api.openweathermap.org/data/2.5/find?q=%s&units=metric&lang=es&mode=json&APPID=%s";

    private static String NOW_ENDPOINT_BYID =
            "http://api.openweathermap.org/data/2.5/weather?id=%s&appid=%s";

    // Dada una ciudad, lanza un nuevo thread asincrono para obtener los datos
    public void getWeather(String ciudad) throws IOException {
        URL url = new URL(String.format(NOW_ENDPOINT, ciudad, APIKEY));
        new GetWeather().execute(url);
    }

    // Método nuevo para llamar por Id
    public void getWeatherById(int idCiudad) throws IOException {
        URL url = new URL(String.format(NOW_ENDPOINT, String.valueOf(idCiudad), APIKEY));
        new GetWeather().execute(url);
    }


    private class GetWeather extends AsyncTask<URL, Integer, Ciudad> {

        @Override
        protected Ciudad doInBackground(URL... params) {
            JSONObject datos = null;
            URL url = params[0];
            HttpURLConnection urlConnection = null;

            Ciudad ciudad = new Ciudad();
            try {
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                datos = convertInputStreamToJSONObject(in);
                Log.i("PRUEBA", datos.toString());

                // obj es el primer y unico objeto contenido en "list".
                JSONObject obj = datos.getJSONArray("list").getJSONObject(0);

                int idServidor = obj.getInt("id");
                Log.i("PRUEBA id", String.valueOf(idServidor));

                double temperatura = obj.getJSONObject("main").getDouble("temp");
                Log.i("PRUEBA temp", String.valueOf(temperatura));

                double temperaturaMinima = obj.getJSONObject("main").getDouble("temp_min");
                Log.i("PRUEBA temp_min", String.valueOf(temperaturaMinima));

                double temperaturaMaxima = obj.getJSONObject("main").getDouble("temp_max");
                Log.i("PRUEBA temp_max", String.valueOf(temperaturaMaxima));

                double humedad = obj.getJSONObject("main").getDouble("humidity");
                Log.i("PRUEBA humedad", String.valueOf(humedad));

                double presion = obj.getJSONObject("main").getDouble("pressure");
                Log.i("PRUEBA presion", String.valueOf(presion));

                double viento = obj.getJSONObject("wind").getDouble("speed");
                Log.i("PRUEBA viento", String.valueOf(viento));

                String codigoTiempo = obj.getJSONArray("weather").getJSONObject(0).getString("icon");
                Log.i("PRUEBA codigoTiempo", String.valueOf(codigoTiempo));
                int imagenTiempo = getDrawableFromString(codigoTiempo);


                ciudad.setCiudad_idservidor(idServidor);
                ciudad.setCiudad_temperatura(String.valueOf(temperatura));
                ciudad.setCiudad_temperatura_minima(String.valueOf(temperaturaMinima));
                ciudad.setCiudad_temperatura_maxima(String.valueOf(temperaturaMaxima));
                ciudad.setCiudad_humedad(String.valueOf(humedad));
                ciudad.setCiudad_presion_atmosferica(String.valueOf(presion));
                ciudad.setCiudad_viento(String.valueOf(viento));
                ciudad.setLink_imagen_tiempo_actual(imagenTiempo);

            } catch (Exception e) {
                Log.e("PRUEBA", e.toString());
            } finally {
                urlConnection.disconnect();
                Log.i("PRUEBA", "finally");
            }

            Log.i("PRUEBA", "...");

            return ciudad;
        }

        // Se ejecuta al obtener una respuesta
        protected void onPostExecute(Ciudad ciudad) {

            // double temperatura = getWeatherConditions(response);
            // Log.i("OPENWEATHERAPI:", String.valueOf(temperatura));

            miCiudad = ciudad;

            //Todo: Llamar al método de InformationDTO que hace el update de la base de datos
        }

    }


    private void updateCiudad(Ciudad ciudad) {

        String query = "UPDATE ciudades SET ___ WHERE idservidor = " + String.valueOf(ciudad.getCiudad_idservidor());


    }


    private int getDrawableFromString(String codigoTiempo) {

        int drawable = -1;

        // Todo: Mapear los iconos
        switch (codigoTiempo) {
            case "02n":
                drawable = R.drawable.sol;
                break;
            default:
                drawable = R.drawable.fog_night;
        }


        return drawable;
    }


    private static JSONObject convertInputStreamToJSONObject(InputStream inputStream)
            throws JSONException, IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return new JSONObject(result);

    }

/*
{
   "count":1,
   "cod":"200",
   "message":"accurate",
   "list":[
      {
         "clouds":{
            "all":0
         },
         "dt":1447155000,
         "coord":{
            "lat":41.38879,
            "lon":2.15899
         },
         "id":3128760,
         "wind":{
            "gust":9.8,
            "speed":3.6,
            "deg":310
         },
         "sys":{
            "country":"ES"
         },
         "name":"Barcelona",
         "weather":[
            {
               "id":800,
               "icon":"01d",
               "description":"cielo claro",
               "main":"Clear"
            }
         ],
         "main":{
            "temp_min":21,
            "temp":21,
            "humidity":60,
            "pressure":1028,
            "temp_max":21
         }
      }
   ]
}
* */


}
