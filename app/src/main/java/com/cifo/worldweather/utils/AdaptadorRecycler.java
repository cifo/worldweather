package com.cifo.worldweather.utils;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cifo.worldweather.R;
import com.cifo.worldweather.information.Ciudad;

import java.util.ArrayList;

/**
 * Created by Alumne on 30/10/2015.
 */
public class AdaptadorRecycler extends RecyclerView.Adapter<AdaptadorRecycler.ClaseHolder> {

    public final ArrayList<Ciudad> items;
    private final int itemLayout;

    public AdaptadorRecycler(ArrayList<Ciudad> items, int itemLayout) {
        this.items = items;
        this.itemLayout = itemLayout;
    }

    @Override
    public ClaseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ClaseHolder(v);
    }

    @Override
    public void onBindViewHolder(ClaseHolder holder, int position) {
        final Ciudad item = items.get(position);
        holder.imagenCiudad.setImageResource(item.getLink_imagen_ciudad());
        holder.nombreCiudad.setText(item.getCiudad_nombre());
        holder.textoGrados.setText(item.getCiudad_temperatura());
        holder.imagenTiempo.setImageResource(item.getLink_imagen_tiempo_actual());

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ClaseHolder extends RecyclerView.ViewHolder {

        public TextView nombreCiudad;
        public TextView textoGrados;
        public ImageView imagenTiempo;
        public ImageView imagenCiudad;

        public ClaseHolder(View itemView) {
            super(itemView);

            nombreCiudad = (TextView) itemView.findViewById(R.id.item_ciudad_txtCiudad);
            textoGrados = (TextView) itemView.findViewById(R.id.item_ciudad_txtGrados);
            imagenTiempo = (ImageView) itemView.findViewById(R.id.item_ciudad_ivTiempo);
            imagenCiudad = (ImageView) itemView.findViewById(R.id.item_ciudad_fotoCiudad);
        }
    }
}
