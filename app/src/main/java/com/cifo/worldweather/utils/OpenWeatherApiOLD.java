package com.cifo.worldweather.utils;

import android.os.AsyncTask;
import android.util.Log;

import com.cifo.worldweather.utils.HttpRequest.HttpRequestException;
import org.json.*;

import java.util.List;


/**
 * Created by juan on 5/11/15.
 */
public class OpenWeatherApiOLD {

    //TODO IMPLEMENTAR PARSEO DE DATOS Y QUE LOS DEVUELVA.

    // key necesaria para obtener datos
    private static final String APIKEY ="1c469181d318bc30f57d87f58af99b2f";

    // Recibe String del tipo "Barcelona,es"
    private static String NOW_ENDPOINT =
            "http://api.openweathermap.org/data/2.5/find?q=%s&units=metric&lang=es&mode=json&APPID=%s";


    // Dada una ciudad, lanza un nuevo thread asincrono para obtener los datos
    public void getWeather(String ciudad) {
            String url = String.format(NOW_ENDPOINT, ciudad, APIKEY);
            new GetWeatherTask().execute(url);
    }

    // Metodo asincrono
    private class GetWeatherTask extends AsyncTask<String, Long, String> {
        // Hace una llamada asincrona
        // cuando obtiene la respuesta salta el metodo onPostExecute
        protected String doInBackground(String... urls) {
          // Log.i("URL", urls[0]);
            try {
                String response = HttpRequest.get(urls[0]).accept("application/json").body();
               // Log.i("RESPONSE", response);
                return response;
            } catch (HttpRequestException exception) {
                return null;
            }
        }

        // Se ejecuta al obtener una respuesta
        protected void onPostExecute(String response) {
            try {

                double temperatura = getWeatherConditions(response);
                Log.i("OPENWEATHERAPI:", String.valueOf(temperatura));


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    // Metodo que deberia parsear el objeto json obtenido
    private double getWeatherConditions(String json) throws JSONException {
        // Ejemplo para obtener la temperatura

        // obj es el primer y unico objeto contenido en "list".
        JSONObject obj = new JSONObject(json).getJSONArray("list").getJSONObject(0);

        // A partir de el podemos obtener lo que queramos
        double latitud = obj.getJSONObject("coord").getDouble("lon");

        double temperatura = obj.getJSONObject("main").getDouble("temp");

        return temperatura;

    }


    /*  Ejemplo de objeto JSON obtenido

   {
    "message":"accurate",
    "cod":"200",
    "count":1,
    "list":[
        {
            "id":3128760,
            "name":"Barcelona",
            "coord":{
                "lon":2.15899,
                "lat":41.38879
            },
            "main":{
                "temp":17,
                "pressure":1030,
                "humidity":93,
                "temp_min":17,
                "temp_max":17
            },
            "dt":1447003800,
            "wind":{
                "speed":4.6,
                "deg":230
            },
            "sys":{
                "country":"ES"
            },
            "clouds":{
                "all":20
            },
            "weather":[
                {
                    "id":801,
                    "main":"Clouds",
                    "description":"algo de nubes",
                    "icon":"02n"
                }
            ]
        }
    ]
}

        */



}
