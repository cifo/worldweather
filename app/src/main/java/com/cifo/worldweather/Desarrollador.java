package com.cifo.worldweather;


import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;


/**
 * Created by Alumne on 23/10/2015.
 */
public class Desarrollador implements Parcelable {
    // Class members
    private int avatar;
    private String nombre;
    private String apellido;
    private String info;
    private String mail;
    private ArrayList<SocialMedia> socialList;
    private static final int LINK_ACTIVO_PNG = R.drawable.ic_link;

    // Constructors
    public Desarrollador(int avatar, String nombre, String apellido, String info, String mail, ArrayList<SocialMedia> socialList) {
        this.avatar = avatar;
        this.nombre = nombre;
        this.apellido = apellido;
        this.info = info;
        this.mail = mail;
        this.socialList = socialList;
    }

    public Desarrollador(String nombre) {
        this.nombre = nombre;
    }

    // Getters
    public int getAvatar() {
        return avatar;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getNombreCompleto() {
        String fullName = String.format("%s %s", nombre, apellido);
        return fullName;
    }

    public String getInfo() {
        return info;
    }

    public String getMail() {
        return mail;
    }

    public int getMailLinkActiveResource() {
        return LINK_ACTIVO_PNG;

    }

    public ArrayList<SocialMedia> getSocialList() {
        return socialList;
    }


    // PARCELABLE METHODS

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Desarrollador> CREATOR = new Creator<Desarrollador>() {
        @Override
        public Desarrollador createFromParcel(Parcel in) {
            return new Desarrollador(in);
        }

        @Override
        public Desarrollador[] newArray(int size) {
            return new Desarrollador[size];
        }
    };

    protected Desarrollador(Parcel in) {
        avatar = in.readInt();
        nombre = in.readString();
        apellido = in.readString();
        info = in.readString();
        mail = in.readString();
        socialList = in.createTypedArrayList(SocialMedia.CREATOR); //LO QUE CONTIENE HA DE SER TB PARCELABLE
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(avatar);
        dest.writeString(nombre);
        dest.writeString(apellido);
        dest.writeString(info);
        dest.writeString(mail);
        dest.writeTypedList(socialList);
    }

}
