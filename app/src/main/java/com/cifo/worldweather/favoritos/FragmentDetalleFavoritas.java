package com.cifo.worldweather.favoritos;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cifo.worldweather.R;
import com.cifo.worldweather.information.Ciudad;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentDetalleFavoritas extends Fragment {

    public FragmentDetalleFavoritas() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.detalle_ciudad, container, false);
    }

    public void setCiudad(Ciudad ciudad){

    }
}
