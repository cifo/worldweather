package com.cifo.worldweather.favoritos;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cifo.worldweather.R;
import com.cifo.worldweather.information.Ciudad;
import com.cifo.worldweather.information.InformationDTO;
import com.cifo.worldweather.splash.ListViewInterface;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentListaFavoritas extends Fragment {

    ArrayList<Ciudad>ciudades;
    ListViewInterface lvInterface;

    public FragmentListaFavoritas() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_lista_favoritas, container, false);

//        InformationDTO.cargarCiudades_por_defecto();
        ciudades = InformationDTO.getCiudades_por_defecto();
        RecyclerView recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view_favoritos);
        recyclerView.setHasFixedSize(true);// Este metodo indica si cambian los datos, no cambiar á
        AdapterRecyclerFavoritas adapter = new AdapterRecyclerFavoritas(ciudades,  R.layout.favorite_layout);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter.SetOnItemClickListener(new AdapterRecyclerFavoritas.OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                lvInterface.onCiudadSelected(ciudades.get(position));


            }
        });
        return v;
    }

    public void setListViewInterface(ListViewInterface lvInterface) {
        this.lvInterface = lvInterface;
    }
}
