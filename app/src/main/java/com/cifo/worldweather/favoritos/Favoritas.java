package com.cifo.worldweather.favoritos;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.cifo.worldweather.Desarrollador;
import com.cifo.worldweather.R;
import com.cifo.worldweather.information.Ciudad;
import com.cifo.worldweather.splash.ListViewInterface;

public class Favoritas extends AppCompatActivity implements ListViewInterface{

    ImageView imagenCiudad;
    Toolbar toolbar;
    AppBarLayout appbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favoritas);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        appbar = (AppBarLayout)findViewById(R.id.appbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        FragmentListaFavoritas flFavoritas = new FragmentListaFavoritas();
        flFavoritas.setListViewInterface(this);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.contentFragmento, flFavoritas);
        ft.commit();
        imagenCiudad = (ImageView) findViewById(R.id.favoritas_imagen_ciudad);
    }

    @Override
    public void onDesarrolladorSelected(Desarrollador d) {

    }

    @Override
    public void onCiudadSelected(Ciudad c) {
//        CoordinatorLayout coordinator = (CoordinatorLayout) findViewById(R.id.coordinator);
        AppBarLayout appbar = (AppBarLayout) findViewById(R.id.appbar);
        CollapsingToolbarLayout colAppBar = (CollapsingToolbarLayout) findViewById(R.id.favoritas_collapsingBar);
        colAppBar.setTitle("Este es el nombre de la ciudad.");
        appbar.setExpanded(true);
//        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) appbar.getLayoutParams();
//        AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) params.getBehavior();
//        int[] consumed = new int[2];
//
//        behavior.onNestedPreScroll(coordinator, appbar, null, 0, -1000, consumed);

        imagenCiudad.setImageResource(c.getLink_imagen_ciudad());

        Fragment detalle = new FragmentDetalleFavoritas();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.contentFragmento, detalle);
        ft.commit();
    }
}
