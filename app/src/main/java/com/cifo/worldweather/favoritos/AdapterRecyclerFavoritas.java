package com.cifo.worldweather.favoritos;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;


import com.cifo.worldweather.R;
import com.cifo.worldweather.information.Ciudad;

import java.util.ArrayList;

/**
 * Created by Alumne on 30/10/2015.
 */
public class AdapterRecyclerFavoritas extends RecyclerView.Adapter<AdapterRecyclerFavoritas.ViewHolder>{

    public final ArrayList<Ciudad> items;
    public final int itemLayout;
    protected AdapterRecyclerFavoritas.OnItemClickListener mItemClickListener;

    public AdapterRecyclerFavoritas(ArrayList<Ciudad> items, int itemLayout) {
        this.items = items;
        this.itemLayout = itemLayout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
            final Ciudad item = items.get(position);

            holder.img.setImageResource(R.drawable.spinner_despejado_dia);
            holder.nombre.setText(item.getCiudad_nombre());
            holder.temperatura.setText(String.valueOf(item.getCiudad_temperatura()) + "º");

        //holder.itemView.setTag(item);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void SetOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mItemClickListener = onItemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public ImageView img;
        public TextView nombre;
        public TextView temperatura;

        public ViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.favoriteL_image_weather);
            nombre = (TextView) itemView.findViewById(R.id.favoriteL_nombreCiudad);
            temperatura = (TextView) itemView.findViewById(R.id.favoriteL_temperaturaCiudad);
            itemView.setOnClickListener(this);

        }
        @Override
        public void onClick(View v) {
                mItemClickListener.onItemClick(v, getPosition());
        }
    }

    public interface OnItemClickListener {
        public void onItemClick(View v, int position);
    }
}
